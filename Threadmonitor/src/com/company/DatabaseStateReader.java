package com.company;

import java.sql.SQLException;
import java.util.ArrayList;

public class DatabaseStateReader implements Runnable {

    ReadDataForChecking readDataForChecking = new ReadDataForChecking();
    private ArrayList<String> previousFileListInDataSource;

    boolean exit = false;

    public DatabaseStateReader() {


        try {
            previousFileListInDataSource = readDataForChecking.readdataforcheck();
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

    public void run() {


        while (!exit) {
            ArrayList<String> newFileListInDataSource = new ArrayList<String>();

            ReadDataForChecking newdataschecking = new ReadDataForChecking();

            try {
                newFileListInDataSource = newdataschecking.readdataforcheck();
            } catch (SQLException e) {
                e.printStackTrace();
            }

            for (String singlefile : newFileListInDataSource) {
                while (!previousFileListInDataSource.contains(singlefile)) {
                    previousFileListInDataSource.add(singlefile);

                    System.out.println(singlefile);
                }
            }


        }
    }
}
